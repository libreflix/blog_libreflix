#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Guilmour Rossi'
SITENAME = u'Libreflix Blog'
SITEURL = ''

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'pt'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'feeds/all.atom.xml'
FEED_ALL_RSS = 'feeds/all.rss.xml'
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

# Blogroll
LINKS =  (('Libreflix', 'http://libreflix.org/'),
	  ('Código-fonte', 'http://libregit.org/libreflix'),)

# Social widget
SOCIAL = (('Twitter', 'https://twitter.com/libreflix'),
	  ('Instagram', 'https://instagram.com/libreflix'),
          ('Facebook', 'https://facebook.com/libreflix'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = "/home/guilmour/gits/blog_libreflix/themes/tuxlite_tbs"

MENUITEMS = (
    ('Início', '/'),
	('Lançamentos', '/c/lancamentos.html'), 	#/c/lancamentos.html
	('Sessões', '/p/construcao.html'),		#/c/sessoes.html
	('Rabiscos', '/c/rabiscos.html'),
	('Arquivo', '/archives.html'),
	('Voluntariado', '/p/construcao.html'),	#/p/voluntariado.html
    ('Na Mídia', '/p/na-midia.html'),
    ('FAQ', '/p/construcao.html'),			#/p/faq.html
    ('Voltar ao Libreflix &rarr;', 'https://libreflix.org'),
)

CATEGORY_SAVE_AS = 'c/{slug}.html'
CATEGORY_URL = 'c/{slug}.html'
PAGES_URL = 'p/{slug}.html'
PAGE_SAVE_AS = 'p/{slug}.html'
