Title: Apresentando o aplicativo para Android 0.1
Date: 2017-11-05 10:00
Category: Desenvolvimento
Tags: apps, android, mobile
Author: Guilmour Rossi

Eba! Saiu o nosso aplicativo para smartphones Android. O aplicativo foi desenvolvido pelo [Kassiano Resende](https://instagram.com/kassianoresende/), e foi criado a partir de [discussões](https://notabug.org/libreflix/libreflix/issues/7) que tivemos lá no Notabug.

A aplicativo funciona como uma webview para o site, que já e responsivo e a experiência fica bem legal para o usuário. O aplicativo vem a calhar também, pois parece que com o webview, não há o [problema](https://notabug.org/libreflix/libreflix/issues/9) que vem acontecendo em muitos navegadores Chrome mobile que não estão reproduzindo as produções.

Para baixar, basta acessar a página de aplicativos no Libreflix, clicando [aqui](https://libreflix.org/apps/#android).

Não se esqueça de habilitar a opção "fontes desconhecidas" no Android, que autoriza a instalação de aplicativos que não estão na loja oficial.



<img src="/images/aplicativo-android.jpg" width="830">

Abraços libres! <3
