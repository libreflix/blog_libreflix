Title: Cultura livre do sul global – um manifesto
Date: 2018-12-21 10:00
Category: Ativismo
Author: Cultura Livre
FbImage: images/sa_1545417556_Manifesto-600x338.png

![](/images/sa_1545417556_Manifesto-600x338.png)

Nascido enquanto movimento mais ou menos organizado a partir da pauta anticopyright, a cultura livre é, para a maior parte da população do sul (e do norte também) global, uma incógnita. Cultura livre é compartilhar cultura nas redes para todes? É acesso livre e gratuito à bens culturais, em licenças que favorecem o compartilhamento? é buscar práticas alternativas ao copyright de remuneração para autorxs e produtorxs de conteúdo? uma crítica à propriedade intelectual que restringe e criminaliza o intercâmbio de cultura, potencializado ainda mais a partir da internet? um movimento social “digital” em prol do conhecimento aberto? uma cultura feita de forma “livre”, sem amarras com movimentos, organizações e quaisquer outros fatores que tornam a cultura presa e fechada?

No Encontro de Cultura Livre do Sul, realizado nos dias 21, 22 e 23 de novembro de 2018 na internet, discutimos e buscamos respostas para algumas destas questões acima descritas e outras mais. Durante as 6 mesas de debate do encontro, das discussões nas plataformas digitais e redes sociais, falamos sobre políticas públicas e marcos legais de direitos do autor; digitalização de acervos e acesso ao patrimônio cultural em repositórios livres; de laboratórios, produtoras colaborativas, hackerspaces, hacklabs e outras formas de organizações que defendem e praticam no dia a dia a cultura livre; de como nos inserimos em uma rede internacional e da questão da defesa dos bens comuns que a cultura livre também faz; das muitas formas de produção cultural – editorial, musical, audiovisual, encontros, fotográficas – que estão sendo realizadas no âmbito das licenças e da cultura livre; e das plataformas, conteúdos e práticas educacionais que tem o livre como paradigma de ação e propagação.

Com os mais de 200 participantes inscritos que tomaram parte desses três dias, pensamos sobre as especificidades da cultura livre no sul global em relação ao norte. A discussão sobre a liberdade de usos e produção de tecnologias livres tem sido fundamental para a cultura livre desde o princípio, mas acreditamos que, no sul, temos a urgência maior de nos perguntar para quê e quem servem nossas tecnologias livres. Não basta somente discutir se vamos usar ferramentas produzidas em softwares livres ou se vamos optar por licenças livres em nossas produções culturais: necessitamos pensar em tecnologias, ferramentas e processos livres que sejam usadas para dar espaço, autonomia e respeito aos menos favorecidos, financeira e tecnologicamente, de nossos continentes, e para diminuir as desigualdades sociais em nossos locais, desigualdades estas ainda mais visíveis no contexto de ascensão fascista global que vivemos nesse 2018.

Desde o sul, temos que pensar na cultura livre como um movimento e uma prática cultural que dialogue intensamente com as culturas populares de nossos continentes; que respeite e converse com os povos originários da América, que estão aqui em nosso continente vivendo em uma cultura livre muito antes da chegada dos “latinos”; que defenda o feminismo e os direitos iguais a todes, sem distinção de raça, cor, orientação sexual, identidade e expressão de gênero, deficiência, aparência física, tamanho corporal, idade ou religião; que dialogue com a criatividade recombinante das periferias dos nossos continentes, afeitas ao compartilhamento comunitário e sendo alvo principal do extermínio praticado por nossas polícias regionais; que busque resguardar nossa privacidade a partir de táticas antivigilância e na defesa do direito ao anonimato e à criptografia; e que lute pela propagação das fissuras no sistema capitalista, buscando, a partir de uma prática cultural e tecnológica anticopyright, formas alternativas e solidárias de vivermos em harmonia com Pachamama sem esgotar os recursos já escassos de nosso planeta.

Pensar e fazer a cultura livre desde o sul requer pensarmos na urgência das necessidades de sobrevivência do nosso povo. Requer nos aproximarmos da discussão sobre o comum, conceito chave que nos une na luta contra a privatização dos recursos naturais, como os oceanos e o ar, mas também dos softwares livres e dos protocolos abertos e gratuitos sob os quais se organiza a internet. Nos aproximar do comum amplia nosso campo de disputa no sul global e nos aproxima do cotidiano de comunidades, centrais e periféricas, que lutam no dia a dia pela preservação dos bens comuns.

Importante lembrar que o conceito de comum do qual buscamos nos aproximar deve ser pensado como algo em processo, como um fazer comum (commoning em inglês). Isto é, não termos em vista somente o produto em si – livro, vídeo, música, hardware ou software livres – mas a nossas próprias práticas e dinâmicas através das quais juntos criarmos novas formas de viver, conviver e também produzir. Este é o fazer comum. Por isso, é tão importante mantermos vivas essas redes que acabamos de ativar, essas conexões que percorreram todas as mesas e todas as plataformas nas quais mapeamos, escrevemos, registramos e gravamos.

Para os próximos anos, nos comprometemos a seguir os esforços de tornar a cultura livre um movimento que, além de lutar por tecnologias, produtos e práticas culturais não proprietárias, também batalhe pela redução da desigualdade social de nossos continentes a partir do ativismo pela liberdade do conhecimento em prol de comunidades mais justas, autônomas, igualitárias, respeitosas e livres. Temos, como objetivos para os próximos 5 anos (2019 – 2024):

- Realizar encontros bianuais, online ou presencial, com o objetivo de desenvolver uma parceria global para o desenvolvimento e defesa da cultura livre e dos bens comuns;

- Alimentar e divulgar mais amplamente as plataformas para o mapeamento e curadoria de iniciativas de cultura livre;

- Criar e manter fóruns online para incentivar o debate e as trocas entre os diferentes projetos/atores de cultura livre do Sul, especialmente no intervalo dos encontros;

- Propor formações contínuas em cultura livre, de modo a relacionar as práticas e conceitos trabalhados à pessoas e projetos do sul global;

- Promover espaços seguros de inclusão e diversidade dentro dos debates sobre cultura livre, garantindo a igualdade de direitos. Em nossos espaços serão rejeitados todos os tipos de práticas e comportamentos homofóbicos, racistas, transfóbicos, sexistas ou excludentes de alguma forma;

- Fortalecer a liberdade de expressão, acesso à informação e a criação de espaços democráticos de comunicação que garantam avanços nas discussões sobre cultura livre e na construção democrática das políticas sobre o tema;

Internet, Ibero-américa, sul-global, 23 de novembro de 2018

Assinam os coletivos:
<br>[BaixaCultura](http://BaixaCultura.org), Brasil
<br>[Casa da Cultura Digital Porto Alegre](http://ccdpoa.com.br/), Brasil
<br>[Ártica](https://www.articaonline.com/), Uruguay
<br>[Ediciones de La Terraza](edicioneslaterraza.com.ar/), Argentina
<br>[Em Rede](http://www.em-rede.com/), Brasil
<br>[Nodo Común](https://nodocomun.org/), Iberoamérica
<br>[Rede das Produtoras Culturais Colaborativas](https://colaborativas.net/), Brasil
<br>[Rede iTEIA.NET](http://www.iteia.org.br/), Brasil
<br>[Libreflix](https://libreflix.org), Brasil
