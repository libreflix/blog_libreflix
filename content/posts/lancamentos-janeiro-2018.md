Title: As produções que chegaram no Libreflix em Janeiro
Date: 2018-01-29 10:00
Category: Lançamentos
Tags: lançamentos, janeiro
Author: Guilmour Rossi

Um novo ano chegou, e agora, todo mês, vamos falar um pouco mais sobre as novas produções que chegaram no catalógo Libreflix. Confira abaixo um pouco mais sobre as novas obras incluidas em Janeiro na plataforma.


## Não Vivamos Mais Como Escravos
<center>
<a href="https://libreflix.org/assistir/nao-vivamos-mais-como-escravos">
<img src="https://libreflix.org/media/96e0ade0-ee54-11e7-9b74-15a5432e2110nao-vivamos-como-escravos_thumb.jpg" width="350px">
</a>
</center>

Sinopse: O documentário produzido em 2013 aborda, a partir de uma perspectiva anarquista, a atual situação social, política e econômica grega, apresentando as causas da crise bem como as estratégias adotadas por coletivos e militantes anarquistas em resposta a esta.

Documentário de Ativismo Social · 2013 · Livre · 1h 29m

Tags: anarquismo, grécia, autogestão, liberdade, igualdade, docs, social, ativismo

## Limite (1931)
<center>
<a href="https://libreflix.org/assistir/limite">
<img src="https://libreflix.org/media/560fc1b0-ee5a-11e7-9b74-15a5432e2110limite_thumb.jpg" width="350px">
</a>
</center>


Sinopse: Em um pequeno barco à deriva, duas mulheres e um homem relembram seu passado recente. Uma das mulheres escapou da prisão; a outra estava desesperada; e o homem tinha perdido sua amante. Cansados, eles param de remar e se conformam com a morte, relembrando as situações de seu passado. Eles não têm mais força ou desejo de viver e atingiram o limite de suas existências.

Longa metragem de Drama/Romance· 1931 · 12 · 1h 57m

Tags: classicos, nacionais, filmes

## Remar é...
<center>
<a href="https://libreflix.org/assistir/remar-e">
<img src="https://libreflix.org/media/cd4451d0-ee3a-11e7-9b74-15a5432e2110remareh_thumb.jpg" width="350px">
</a>
</center>

Sinopse:  documentário "Remar é..." faz um passeio pelos centenários clubes de regata do Rio de janeiro ... atletas de ontem e de hoje , As glórias do passado, conquistas, feitos incríveis e eternos amantes do remo...A natureza e o remo...uma crônica apaixonada!

Documentário de Esporte · 2013 · L · 1h15m

Tags: esporte, docs, natal, rio de janeiro, botafogo, flamengo, vasco, remo

## Onde Começa Um Rio
### Em defesa da universidade pública para todxs!
<center>
<a href="https://libreflix.org/assistir/onde-comeca-um-rio">
<img src="https://libreflix.org/media/63725aa0-fe10-11e7-8b2c-47af411d5df3ondecomecaumrio-thumb.jpg" width="350px">
</a>
</center>

Sinopse: "Onde começa um rio" se volta para as ocupações universitárias contra a PEC do Fim do Mundo no final de 2016. O filme desacelera as turbulências diárias para ouvir a voz dxs ocupantes, saber quem são, de onde vem, o que pensam, ouvi-lxs refletir sobre suas experiências, desejos, conflitos e leituras do país.

Documentário de Ativismo Social · 2017 · L · 1h 12m

Tags: ativismo, movimento estudantil, ocupações, educação

## O Nostalgista
<center>
<a href="https://libreflix.org/assistir/o-nostalgista">
<img src="https://libreflix.org/media/ec5f9120-e777-11e7-9b74-15a5432e2110download.png" width="350px">
</a>
</center>

Sinopse: Em uma cidade do futuro, um pai precisa pegar estrada em busca de uma reposição para seu dispositivo de realidade virtual.

Curta metragem de Ficção-Científica · 2014 · Livre · 17m

Tags: sci-fi, curtas, tech, cyberpunk

## Por que meu cabelo não é liso?
<center>
<a href="https://libreflix.org/assistir/porquemeucabelonaoeliso">
<img src="https://libreflix.org/media/5e8f36c0-ee39-11e7-9b74-15a5432e2110pqmeucabelo_thumb.jpg" width="350px">
</a>
</center>

Sinopse: Muitos cresceram escutando que tinham cabelos "ruins", que deviam parecer diferente, pentear. "Seu cabelo é ruim", "por que você não alisa esse cabelo?", Uma menina descobre um outro lado de si mesma após reconhecer sua identidade através do espelho. Contando com depoimentos reais e histórias que irão tocar você. Descubra a força e a beleza de suas raízes!

Curta metragem documentário/ficção · 2015 · Livre · 21m

Tags: feminismo, emponderamento, fic-doc, drama, cabelo, cabelo crespo, docs

## Laços
<center>
<a href="https://libreflix.org/assistir/lacos">
<img src="https://libreflix.org/media/ff234a70-f0b1-11e7-9b74-15a5432e2110lcaos_thumb.jpg" width="350px">
</a>
</center>

Sinopse: Laços” conta a história de personagens femininas que estão mais próximas de nós do que imaginamos. São nossas amigas, nossas irmãs, vizinhas, são mulheres da nossa família e do nosso convívio social. Um laço pode parecer um enfeite, mas também é capaz de amarrar, amordaçar e prender. A luta feminina é muito maior do que se pensa. Às vezes essa luta, muitas vezes silenciosa, é para desfazer um nó que prende o presente ao passado e impede de prosseguir. Ficção baseada em histórias reais, de mulheres reais que em algum momento da vida se viram presas a histórias que não queriam viver.

Curta metragem documentário/ficção · 2016 · 14 Anos · 26m

Tags: ficção, violência, feminismo, emponderamento, curtas

## Putta
<center>
<a href="https://libreflix.org/assistir/putta">
<img src="https://libreflix.org/media/364b73a0-e794-11e7-9b74-15a5432e2110um_putta_thumbnail.jpg" width="350px">
</a>
</center>

Sinopse: Putta acompanha o relato biográfico de três mulheres da fronteira Brasil, Paraguay e Argentina. As três vivem em Foz do Iguaçu (Brasil) e trabalham no ambiente da prostituição. O filme atravessa as complexidades da vida pessoal destas três prostitutas, que trabalham na rua ou em casas de prostituição, a transexualidade, a família e a maternidade nestes contextos. Diva, uma doce mulher trans, nos leva a sua casa para recordar suas difíceis relações com a família que deixou para trás e a que quis formar. Xayenne, uma travesti, que sempre tenta dar a volta em suas memórias e não reviver seus sofrimentos. Pantera, dona de um bordel, vive há 40 anos em prostíbulos, no mesmo bairro, da mesma cidade, onde criou a seus filhos.

Documentário de curta metragem · 2016 · 12 Anos · 28m

Tags: docs, feminismo, prostitutas, transexualidade, biografia, curtas



## Noção de Sonho
<center>
<a href="https://libreflix.org/assistir/nocao-de-sonho">
<img src="https://libreflix.org/media/f3011b60-fe34-11e7-8b2c-47af411d5df3nocao-de-sonho-thumb.jpg" width="350px">
</a>
</center>

Sinopse: Após a morte de sua mãe, Marta acaba se rendendo ao uso de calmantes e antidepressivos para superar a perda. O que a leva a um conflito interno entre o que é realidade e sonho, tendo que enfrentar Marcos e descobrir se ele existe ou é só fruto de sua mente perturbada.

Curta  · Suspense · 2017 · 10 · 15min

Tags: curtas, suspense, noção de sonho, sonho, pesadelo, mente perturbada, roubo, assalto, psicológico, thriller psicológico
