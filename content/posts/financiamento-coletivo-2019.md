Title: Está no ar nossa vaquinha para 2019
Date: 2019-05-09 10:00
Category: Avisos
Tags: apps, financiamento, mobile
Author: Guilmour Rossi

Ano passado vocês ajudaram a gente a custear 475 reais para pagar o servidor. Bem, nesse ano nossos voos serão um pouquinho mais altos e contínuamos precisamos da sua ajuda pra fazer uma plataforma de streaming, gratuita, sem anúncios, sem cadastros, sem fins lucrativos e de código-fonte aberto. 🎉


Ajude a construir o Libreflix a partir de 1 REAL. A doação pode ser única ou recorrente, você que manda.
Acesse: [https://acredito.me/libreflix](https://acredito.me/libreflix)

![](/images/media/2019/libreflix-no-celu.jpg)
