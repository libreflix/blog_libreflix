Title: Confira o novo aplicativo para Windows
Date: 2017-11-19 10:00
Category: Desenvolvimento
Tags: apps, windows
Author: Guilmour Rossi

Agora ficou mais fácil assistir obras audiovisuais livres em computadores **Windows**.

O aplicativo pode ser instalado e cria ícones no seu desktop e no menu iniciar, facilitando assim que você abre o Libreflix mais rápido.

O aplicativo usa o Chrome para abrir o site então para funcionar você precisa ter o Chrome instalado em seu computador Windows.

Para fazer o download, basta acessar a página de aplicativos do Libreflix clicando [aqui](https://libreflix.org/apps/#windows).

Infelizmente, parece que alguns poucos antívirus para Windows estão detectando o arquivo como uma ameaça, não sabemos ainda porque, mas garatimos que não há nenhum vírus no programa. Confira a [análise](https://www.virustotal.com/#/file/72c932762d5b49700ef7573d84dd61874a02bca493b2ae7b1f991dd66de3ab46/detection) feita pelo Virus Total.


<center>
<a href="https://libreflix.org/apps/#windows"><img src="/images/app-windows01.jpg" width="512"></a>
</center>
