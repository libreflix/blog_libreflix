Title: Olá, mundo!
Date: 2017-08-22 10:00
Category: Avisos
Author: Guilmour Rossi

Oi, amigos e amigas. Queremos apresentar para vocês o LIBREFLIX.

O Libreflix é uma plataforma de streaming aberta e colaborativa, que reúne produções audiovisuais independentes e/ou de livre exibição. A ideia também é difundir produções locais e apoiar o movimento da cultura livre [1].

Ela é colaborativa pois qualquer um pode adicionar um novo filme, documentário, etc., seja um usuário que conheça uma produção assim ou até o criador do conteúdo.

Ela é aberta pois tem seu código-fonte aberto, ou seja, qualquer um pode ver como o programa funciona por dentro e também pode ajudar a melhorá-lo. [2]
Quem quiser conferir, chega mais: [Libreflix.org](https://libreflix.org)

- [1]: [https://pt.wikipedia.org/wiki/Cultura_livre](https://pt.wikipedia.org/wiki/Cultura_livre)
- [2]: [https://notabug.org/libreflix](https://notabug.org/libreflix)


<img src="/images/ola-mundo.jpg" width="830">

Abraços libres! <3
