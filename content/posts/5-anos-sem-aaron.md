Title: 5 Anos sem Aaron Swartz
Date: 2018-01-11 10:00
Category: Ativismo
Tags: aaron, cultura livre
Author: Guilmour Rossi

Hoje faz 5 anos que perdemos Aaron Swartz. Seu legado continua vivo em todos nós:

**CONHECIMENTO PRECISA SER LIVRE!**

Para saber mais sobre a história e a luta de Aaron, e as injustiças que ele sofreu, assista o documentário "O Menino da Internet", no Libreflix: [https://libreflix.org/assistir/the-internets-own-boy](https://libreflix.org/assistir/the-internets-own-boy)

<center>
<img src="/images/media/2018/aaron-5-anos.jpg" width="470px">
</center>
