Title: 5 filmes para entender mais sobre as ocupações urbanas
Description: Listados a seguir estão alguns documentários no Libreflix para você ver mais de perto essas lutas
Date: 2018-05-05 10:00
Category: Listas
Author: Karolyna Gutierres
FbImage: /images/media/2018/17ac1170-50b7-11e8-ba38-9bd0820aa2aaphoto_2018-05-05_19-52-24.jpg

<p align="right"><small> Por Karolyna Gutierres, Guilmour Rossi e Marcielo de Moraes </small></p>
No dia 1 de maio, em São Paulo, um prédio que servia de abrigo para mais de 150 famílias desabou. Logo o desastre emergiu nos meios de comunicação de massa e retomou uma discussão já muito antiga das grandes metrópoles: o problema habitacional.

Especulação imobiliária, falta de planejamento urbano, distribuição espacial irregular, desemprego, concentração de imóveis nas mãos dos grandes proprietários, desigualdade e falta de regulamento no valor dos aluguéis são alguns dos problemas socioambientais que dificultam o direito à moradia para a maior parte da população em grandes cidades e periferias pelo mundo. Uma das alternativas é a ocupação.

Prédios abandonados, muitas vezes pelo próprio governo, servem de refúgio para milhares de famílias que se juntam para otimizar o espaço de forma política e social. É certo que casos como esse último da capital paulista poderiam ser evitados se o Estado não fosse negligente ao investir na desocupação desses espaços ao invés de fomentar políticas públicas que garantissem o mínimo de dignidade a essa população.

Ocupar é sim resistir! Diante desse cenário urbano caótico, ocupar é também tentar sobreviver.

Listados a seguir estão alguns documentários no Libreflix para você ver mais de perto essas lutas:

## 1 - Leva (2011)

![](/images/media/2018/17ac1170-50b7-11e8-ba38-9bd0820aa2aaphoto_2018-05-05_19-52-24.jpg)
<p align="right"><small> Foto: (CC BY-SA) Fora do Eixo </small></p>

No coração de São Paulo pulsa o maior movimento de luta por moradia da América Latina. Famílias desabrigadas ocupam o edifício Mauá, um dentre muitos ocupados no centro da cidade., O documentário LEVA acompanha a vida de moradores da ocupação e revela a organização de siglas que se unem numa organização para transformar os espaços abandonados em habitáveis. A estruturação do edifício pelos movimentos de luta de moradia irá refletir na reorganização e redescoberta das pessoas como indivíduos através do coletivo.

Leva (2011) <br>
Dirigido por Juliana Vicente e Luiza Marques <br>
2011 · 12 · 55m

[Assistir no Libreflix >>](https://libreflix.org/i/leva)

## 2 - Dandara - enquanto morar for um privilégio, ocupar é um direito (2014)

![](/images/media/2018/4b8bf940-50b4-11e8-ba38-9bd0820aa2aapor-do-sol-dandara.jpg)

O longa documentário, dirigido pelo argentino Carlos Pronzato, conta a história da Ocupação Dandara, até então a maior ocupação das Minas Gerais. O filme usa de depoimentos dos moradores, apoiadores e militantes para mostrar a árdua luta contra o capital especulativo.

Dandara (2014) <br>
Dirigido por Carlos Pronzato <br>
Enquanto morar for um privilégio, ocupar é um direito · 2014 · 10 · 1h 05m

[Assistir no Libreflix >>](https://libreflix.org/i/dandara)

## 3 - Atrás da Porta (2010)

![](/images/media/2018/6111dd60-50ba-11e8-ba38-9bd0820aa2aaatras.jpg)


O documentário Atrás da Porta registra a experiência de arrombar prédios e criar novos espaços de moradia das famílias sem-teto do Rio de Janeiro. O filme expõe também uma série de despejos forçados pelo Estado e como esses despejos são o início de uma das maiores intervenções na cidade. No documentário, o projeto chamado de “revitalização” é questionado pelos próprios moradores de várias ocupações.

Atrás da Porta (2010) <br>
Dirigido por Vladimir Seixas e Chapolim <br>
2010 · 10 · 1h 32m

[Assistir no Libreflix >>](https://libreflix.org/i/atrasdaporta)

## 4 - À Margem do Concreto (2006)

![](/images/media/2018/0a569e90-50bd-11e8-ba38-9bd0820aa2aaphoto_2018-05-05_20-34-37.jpg)
<p align="right"><small> Foto: Marcelo Camargo/Agência Brasil</small></p>

Um documentário sobre os sem-teto e os movimentos de moradia em São Paulo. O filme acompanha a atuação de várias lideranças que promovem atos de ocupação na região central de São Paulo e que estão fazendo justiça social com as próprias mãos.

À Margem do Concreto (2006) <br>
Dirigido por Evaldo Mocarzel <br>
2006 · Libre · 1h 26m

[Assistir no Libreflix >>](https://libreflix.org/i/amargemdoconcreto)

## 5 - Por Um Sonho Urbano (2014)

![](/images/media/2018/f5621110-50ce-11e8-ba38-9bd0820aa2aaphoto_2018-05-05_22-43-04.jpg)

Por um sonho urbano irá contar a história das mais de vinte famílias que moram na Ocupação Saraí, um prédio abandonado no Centro de Porto Alegre, entre as ruas Caldas Júnior e Mauá. O documentário irá retratar o dia a dia dos moradores, as suas atividades de rotina, o funcionamento do coletivo e as ações de luta pelo direito de morar à luz do movimento nacional de luta pela moradia.

Por Um Sonho Urbano (2014) <br>
Dirigido por Edye Wilson e Gisele Gonçalves <br>
2014 · Livre · 19m

[Assistir no Libreflix >>](https://libreflix.org/i/por-um-sonho-urbano)
