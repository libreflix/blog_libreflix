Title: Apresentando o nosso campo de busca
Date: 2018-12-5 10:00
Category: Desenvolvimento
Author: Guilmour Rossi
FbImage: images/campo-de-busca.jpg

![](/images/campo-de-busca.jpg)

Olá! Agora o Libreflix conta com um sistema de busca interno.

Um sistema de busca é um item quase que obrigatório em qualquer aplicação, grande ou pequena, hoje em dia. E mesmo com algumas [discussões](https://notabug.org/libreflix/libreflix/issues/40), ainda não tínhamos conseguido implementar uma busca para o projeto, devido à falta de experiência nessa área, mas principalmente pelo custo computacional que isso representa – e consequentemente verba para manter a infraestrutura.

A oportunidade e a sinergia para a implementação desse recurso veio nesse semestre junto com o apoio e as dicas preciosas do professor da UTFPR [Luiz Celso Gomes Jr](http://dainf.ct.utfpr.edu.br/~gomesjr/).

Para o desenvolvimento foi usado primordialmente a *framework* [elasticsearch](https://www.elastic.co/products/elasticsearch), que é uma excelente e escalável ferramenta para realizar essa tarefa. Para isso, tivemos que alugar um novo pequeno servidor de 2GB (+R$40 mensais), mesmo que na [documentação](https://www.elastic.co/guide/en/elasticsearch/guide/current/hardware.html#_memory) a Elastic sugira uma máquina de 32GB, sendo menos de 8GB “contra-produtivo”. Vamos ver como saímos. Mais alguns detalhes dessa implementação podem ser conferidos [aqui](https://speakerdeck.com/guilmour/implementacao-de-um-mecanismo-de-busca-textual-na-plataforma-libreflix-usando-elasticsearch).

Use e abuse desse recurso. Dê [feedback](https://libreflix.org/contato) e começe e/ou se junte às nossas discussões no [repositório](https://libregit.org/libreflix/libreflix/issues). Logo vem mais novidade por aí.

Abraços livres! <3 <br>