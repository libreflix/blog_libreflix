Title: Orgulho 2017
Date: 2017-09-22 10:00
Category: Rabiscos
Author: Guilmour Rossi

Fizemos um doodle para não esquecermos da liberdade; e dizer que além disso, também temos orgulho.

<img src="/images/rabiscos/libreflix_pride.png">
