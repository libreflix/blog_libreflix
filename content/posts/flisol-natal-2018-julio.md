Title: Apresentando o Libreflix no Flisol - Natal 2018
Description:
Date: 2018-05-01 10:00
Category: Eventos
Author: Júlio Lira
FbImage: /images/media/2018/julio_flisol2018_1.jpg

<p align="right"><small> Por Júlio Lira </small></p>

No último fim de semana, no dia 28, eu falei um pouquinho sobre o projeto na edição potiguar do Festival Latino-americano de Instalação de Software Livre.

![](/images/media/2018/julio_flisol2018_1.jpg)

Falar para uma pessoa: "Tal pessoa falará sobre Libreflix" faz transparecer que estaria anunciando um produto ou um software, mas o meu objetivo foi mais do que isso. Eu queria mostrar a profundidade que esse nome carrega, ela não é apenas um site de *streaming*, ela é uma filosofia, e mais, ela é uma comunidade de voluntários que mostra a voz de novas iniciativas de filmes, novos documentários, e consegue até mesmo quebrar as barreiras econômicas e sociais, dando o melhor possível serviço de *streaming* gratuitamente, sem análise suja de dados, respeitando o usuário com software livre e uma comunidade inclusiva e respeitosa. Por isso senti o peso de falar sobre algo tão profundo e além disso, assumo, senti medo de retaliação contra nossas ideias.

Ao marcar o horário notei que a organização me pôs para ser um dos primeiros a falar. Eles até pinaram no grupo responsável pelo evento a palestra "Libreflix.org", e a organização [potilivre](http://potilivre.org/) foi totalmente solidária; ao lerem minha submissão de palestra ficaram bastante interessados (não apenas pelo site, mas por toda mensagem que levamos), e deram total apoio e espaço a palestra em que eu iria falar. 

Isso me deu bastante animo, pelo contrário de o que eu pensava eles não me bloquearam de nada - na verdade me ajudaram! Eu notei que existem ainda organizações que lutam pela mesma ideia, visam o mesmo objetivo, e isso foi o necessário para falar impolgadamente ao [Guilmour](http://guilmour.org/) sobre a palestra e sobre a aceitação e boa recepção que tive. &hearts;

![](/images/media/2018/julio_flisol2018_3.jpg)

Como eu era um dos primeiros a palestrar fiz o possível para não chegar atrasado, mas ao chegar estava bem próximo do horário da minha palestra. Notei que todos estavam bastante atentos, e logo chegou a vez de exercer a grande responsabilidade de retratar o máximo possível o que é a Libreflix. 

Me chamou bastante atenção que ao abrir o slide e iniciar a palestra um dos organizadores chamado [José Roberto](http://potilivre.org/joseroberto) me fez uma pergunta: *"Afinal, se fala "Laibre-flix" ou "Libre-flix"?*. Ele me perguntou porque eu havia falado com uma fonética de "laibre", e isso me fez pensar enquanto eu falava sobre ela. 

Fiquei espantando comigo mesmo, no momento. *Como eu nunca tinha parado para pensar nisso?* É claro, o nome Libreflix vem de libre, liberdade, ser livre... Essa ficha ter caído só naquela hora vence ainda a própria ideia de que eu tinha da Libreflix e me deu ainda mais entusiasmo para apresentar o projeto.


Enquanto falava, pensei sobre a liberdade e o sentido profundo que não só a comunidade Libreflix carrega mas como outras comunidades e organizações que possuem essa visão de mudança e sabem que é possível mudar para melhor o mundo; com justiça, igualdade. 

E o mais poderoso: a capacidade que temos com a nossa união. Pude ter a honra de falar e convidar a outros desenvolvedores para que venham participar de nossa comunidade.

E mesmo se senti nervosismo, notei que aquilo era algo novo a maioria, e o que foi mais gratificante de tudo isso foi terminar a palestra e ver várias pessoas entrando em nosso grupo no telegram querendo conhecer e participar da nossa família. &hearts;

Os slides da apresentação podem ser encontrados clicando [aqui](/images/media/2018/libreflix_org_julio_flisol2018.pdf).

<img src="/images/media/2018/julio_flisol2018_2.jpg" width="470px">

