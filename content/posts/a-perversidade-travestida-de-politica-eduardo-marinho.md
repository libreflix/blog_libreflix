Title: A perversidade travestida de política
Description: Resenha escrita por Eduardo Marinho
Date: 2018-08-04 10:00
Category: Resenhas
Tags: resenhas, eduardo marinho, privatizações, silvio tendler
Author: Eduardo Marinho
FbImage: images/media/2018/por_tao_poucos_pb.jpg

<p align="right"><small> Resenha <a href="http://observareabsorver.blogspot.com/2018/07/privatizacoes-como-perversidade.html"> publicada originalmente </a> no blog Observar e Absorver de Eduardo Marinho </small></p>

![](/images/media/2018/por_tao_poucos_pb.jpg)

A privatização como perversidade. Como um processo estratégico de dominação mundial pela economia, pelos poucos donos do mercado, de todas as áreas por extensão, até mesmo na formação de mentalidades favoráveis a este sistema social anti-humano, anti-mundo, criador e estimulador do inferno na terra, competitividade, consumo excessivo, conflitos, destruições até o genocídio.

Mais uma pérola cinematográfica de Sílvio Tendler, em informações e responsabilidade social, um artista que cumpre sua função, que põe sua sensibilidade na lapidação social, no acendimento de luzes tão necessário nesta realidade trevosa. Estamos aprendendo a ver como a estrutura social funciona, em gerações contemporâneas, como somos impregnados de consentimento, pela sujeição da administração pública aos poucos podres de ricos internacionais com as elites locais, através do modelo de ensino empresarial e, sobretudo, da mídia também empresarial.

O ensino empresarista é competitivo, formador de peças pro mercado de trabalho e não de seres humanos pra compor uma sociedade harmônica, sem abandono, onde todos tenham seus direitos constitucionais, humanos e sociais respeitados por inteiro. Não se encontram escolas que eduquem no sentido da harmonia, da paz de espírito, da verdadeira realização pessoal, social e humana. E isso é interesse de toda a sociedade, ainda que não se pense nisso, ainda que a inconsciência prevaleça.

E a mídia, implantada e enraizada na "cultura nacional" de 1964 até 69, quando virou "rede nacional", exatamente por estar na cabeça do apoio ao golpe militar, exerce o massacre publicitário-ideológico permanente em defesa de interesses empresariais, criando mundos de fantasia, usando psicologia do inconsciente, distorcendo notícias, criando mentalidades que apóiem ideologia do consumo, da competição, do punitivismo, da sabotagem em todo investimento na própria população e nos seus direitos constitucionais.

Não posso deixar de falar de uma meia-discordância no final do filme. "O momento é agora" é um jargão antigo que prepara a desilusão pra depois, muitas vezes até o desânimo. Todos os momentos são agora, o conjunto de todos os agoras forma a eternidade. Prefiro ir espalhando o que tá escondido, falando o que tô vendo, como por toda minha vida, desde os dezenove anos, refletindo, causando reflexão, aprendendo com as práticas cotidianas. Escolher os próprios caminhos, desentranhar condicionamentos, chacoalhar os valores pra ver quais são implantados e quais podem ser verdadeiros, junto com desejos, com objetivos de vida, no que me traz paz ou me inferniza e, a partir disso, fazer escolhas, tomar decisões e levar à prática cotidiana. A história caminha com passo próprio, é preciso medir o passo com ela - sabendo que ela também pode dar seus pulos. Vigiemos.


"Privatizações - A Distopia do Capital" do diretor Sílvio Tendler está disponível gratuitamente no Libreflix.

<a href="https://libreflix.org/i/privatizacoes-a-distopia-do-capital"> Clique aqui para assistir >>> <a/>