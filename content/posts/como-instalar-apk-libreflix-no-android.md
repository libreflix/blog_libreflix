Title: Como instalar o aplicativo Libreflix usando o .apk no Android
Date: 2018-05-23 10:00
Category: Desenvolvimento
Tags: apps, android, mobile, apk
Author: Guilmour Rossi
FbImage: /images/media/2018/clicar-em-salvar-como.jpg

<p align="right"><small> Por Guilmour Rossi e Michael </small></p>

O aplicativo Libreflix para celulares Android já foi lançado há algum tempo, mas algumas dúvidas surgem na hora de instalar o app usando o arquivo .apk.

Esse é um breve tutorial para te ajudar na instalação se você estiver tendo dificuldades.

As dois principais problemas enfrentados são que o Android bloqueia a instalação fora da loja oficial Google Play, mas você pode desmarcar essa opção nas configurações. Outro problema ocorre ao tentar fazer o download do .apk pelo Chrome, onde ele também bloqueia a instalação ao menos que você diga expressamente que quer fazer o download do arquivo. Vamos lá...

#### Fontes Confiáveis

![](/images/media/2018/fontes-desconhecidas.jpg)

Primeiro, vamos autorizar a instalação pelo arquivo .apk.

1. No seu Android, vá em **Configurações** ou **Configurar**;
2. Em **Configurações**, logo em seguida clique em **Segurança**;
3. Em seguida, ative as permissões de **Fontes Desconhecidas**;


#### Fazer o download do Link

![](/images/media/2018/clicar-em-salvar-como.jpg)

Agora, vamos fazer o download do arquivo.

1. Acesse do seu navegador o endereço [Libreflix.org/apps/](http://libreflix.org/apps/#android)
2. Estando na página, encontre o *banner* **"Download para Android"**;
3. Segure em cima do *banner* até que apareça mais opçãos;
4. Clique em **"Fazer download do link"**;
5. O download começará. Quado terminar, clique em **"Abrir"**;
6. E agora escolha **"Instalar".**

Pronto, seu app já está instalado e você pode curtir filmes livres em seu celular.

Qualquer dúvida, deixe-nos saber.

Abraços libres! <3
