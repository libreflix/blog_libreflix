Title: Metas para 2019: versão 1.2 e 1.3
Date: 2019-04-19 10:00 
Category: Desenvolvimento
Tags: roadmap 
Author: Guilmour Rossi

<center>
<img src="/images/media/2019/text4520.png" width=270px>
</center>

Já passamos a virada do ano, o segundo começo do ano (o carnaval) e o outro feriado depois deste, mas ainda não é tarde para traçarmos metas para o ano de 2019.

Pois bem, seguindo a construção coletiva de ideias no [NotABug](https://notabug.org/libreflix/libreflix/issues) e no [LibreGit](https://libregit.org/libreflix/libreflix/issues), chegamos a alguns [objetivos](https://libregit.org/libreflix/libreflix/milestones) de funcionalidades que queremos implementar esse ano, e nos levarão as versões 1.2 e 1.3 do Libreflix! 😄

Dá para resumir tudo em:

- Melhor classificação e separação do acervo
- Funcionalidades para a interação entre usuário e obra. (Comentários, avaliação, opções de "já vi", "favorito" e "assistir mais tarde").
- Funcionalidade para o download das obras
- Fornecer opções de legenda para o player de vídeo
- Área de gerenciamento de conteúdo para usuários e moderação
- Internacionalização básica e tradução da interface para Espanhol e Inglês
- Campo de busca mais avançado
- Versão offline para redes comunitárias
- Streaming de vídeo via redes peer-to-peer (P2P) 


Tomara que cheguemos lá. Não deixe de ficar ligado, pois além disso, temos muitos outros planos que iniciam-se neste 2019 - que todos já percebemos, é o ano das lutas.

