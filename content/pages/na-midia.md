Title: Na Mídia
Status: hidden

O **Libreflix** já foi apresentado em **dezenas** de publicações online, jornais impressos e até em um programa de TV, incluido **Canaltech**, **Revista Fórum**, **Hypeness** e **Nexo Jornal**. <3 

Confira essas e outras matérias nos links abaixo.

###  2017

- Canaltech: [Conheça o Libreflix, serviço de streaming que segue o conceito de cultura livre](https://canaltech.com.br/entretenimento/conheca-o-libreflix-servico-de-streaming-que-segue-o-conceito-de-cultura-livre-102678/)
- Revista Fórum: [Libreflix: A alternativa ao Netflix, colaborativa, gratuita e que faz pensar](https://www.revistaforum.com.br/2017/09/25/libreflix-alternativa-ao--netflix-colaborativa-gratuita-e-que-faz-pensar/)	
- Linux Descomplicado: [Libreflix – plataforma de streaming aberta e colaborativa](https://news.linuxdescomplicado.com.br/2017/09/libreflix-plataforma-de-streaming-aberta-e-colaborativa/)
- Nexo Jornal: [Como funciona o ‘Libreflix’, uma plataforma de vídeo aberta e colaborativa](https://www.nexojornal.com.br/expresso/2017/11/27/Como-funciona-o-‘Libreflix’-uma-plataforma-de-vídeo-aberta-e-colaborativa)
- Hypeness: [7 títulos para assistir na Libreflix, plataforma de streaming colaborativa e gratuita
](http://www.hypeness.com.br/2017/11/7-titulos-para-assistir-na-libreflix-plataforma-de-streaming-colaborativa-e-gratuita/)
- Diário de Pernambuco: [Estudante cria o Libreflix, a Netflilx gratuita da cultura para obras de arte](http://www.diariodepernambuco.com.br/app/noticia/viver/2017/11/06/internas_viver,729729/libreflix-artistas-plataforma-gratuito-filmes.shtml)
- Partido Pirata: [LIBREFLIX: plataforma aberta e colaborativa](https://partidopirata.org/libreflix-plataforma-aberta-e-colaborativa/)
- Windows Club: [Libreflix, serviço de streaming com filmes e séries gratuitos que você precisa conhecer](https://www.windowsclub.com.br/libreflix-servico-de-streaming-com-conteudo-gratuito-que-voce-precisa-conhecer/)
- Update or Die!: [Libreflix - Uma plataforma aberta de produções independentes](https://www.updateordie.com/2017/11/28/libreflix-uma-plataforma-aberta-de-producoes-independentes/)
- O Beijo: [Conheça Libreflix, o streaming gratuito e colaborativo](http://www.obeijo.com.br/noticias/conheca-libreflix-o-streaming-gratuito-e-colaborativo-12774491)
- Terra: [Conheça o Libreflix, serviço de streaming que segue o conceito de cultura livre](https://www.terra.com.br/noticias/tecnologia/canaltech/conheca-o-libreflix-servico-de-streaming-que-segue-o-conceito-de-cultura-livre,6ced6c94b9ce0534184b2e8883ef0738l0t008g1.html)
- O Comprimido: [Conheça o “libreflix”, alternativa gratuita ao netflix](https://www.ocomprimido.com/dose-diaria/conheca-o-libreflix-alternativa-gratuita-ao-netflix/)
- Educação Integral: [3 plataformas de conteúdo audiovisual com potencial educativo para além do Netflix](http://educacaointegral.org.br/reportagens/3-plataformas-de-conteudo-audiovisual-com-potencial-educativo-para-alem-do-netflix/)
- Vitrini do Cariri: [Conheça o Libreflix, a streaming brasileira que distribui obras sem custos](http://vitrinedocariri.com.br/index.php?p=noticia_int&id=45655)
- Rizoma: [Libreflix: Contenidos audiovisuales abiertos de alto nivel](http://rizoma.facultadlibre.org/libreflix-contenidos-audiovisuales-abiertos-alto-nivel/)
- Notícias ao Minuto: [Conheça o Libreflix, plataforma gratuita com filmes independentes](https://www.noticiasaominuto.com.br/cultura/482949/conheca-o-libreflix-plataforma-gratuita-com-filmes-independentes)
- Digital Arte: [LibreFlix, conhece? Deveria...](http://revistadigitalart.blogspot.com.br/2017/09/libreflix-conhece-deveria.html)
- I'Mídia: [Libreflix: uma plataforma colaborativa e de conteúdo audiovisual livre](https://portalimidia.wordpress.com/2017/11/18/libreflix-uma-plataforma-colaborativa-e-de-conteudo-audiovisual-livre/)
- Prosa Verso e Arte: [Conheça o Libreflix, serviço de streaming aberta, colaborativa e gratuita](http://www.revistaprosaversoearte.com/conheca-o-libreflix-servico-de-streaming-aberta-colaborativa-e-gratuita/)
- Base10: [A alternativa ao Netflix, colaborativa, gratuita e que faz pensar](http://base10.com.br/libreflix-a-alternativa-ao-netflix-colaborativa-gratuita-e-que-faz-pensar/)
- MegaBuzz: [Conheça o Libreflix, serviço de streaming que segue o conceito de cultura livre](http://megabuzz.com.br/conheca-o-libreflix-servico-de-streaming-que-segue-o-conceito-de-cultura-livre/)
- Pueblo y Sociedade: [Conozca el Libreflix, un servicio de streaming que sigue el concepto de cultura libre](http://pysnnoticias.com/conozca-el-libreflix-un-servicio-de-streaming-que-sigue-el-concepto-de-cultura-libre/)
- Ampla Rede: [Assistir filmes online grátis](http://amplarede.com.br/entretenimento/assistir-filme)
- Radio Tube: [Libreflix: A alternativa ao Netflix, colaborativa, gratuita e que faz pensar](https://www.radiotube.org.br/texto-167jTXuu6itZ)

Você também escreveu sobre o Libreflix? Insira seu link editando essa página - é só clicar no link no rodapé. 










### Entrevistas

Se você tiver algum dúvida ou quiser fazer uma entrevista não deixe de entrar em contato conosco.


### Resuminho

- Foi ao ar oficialmente em 11 de Agosto de 2017
- Código-aberto e sem fins lucrativos
- Gratuito para usar hoje e sempre e operando pelos fundamentos do software livre e cultura livre
- Mantida por [@guilmour](http://guilmour.org) e por uma incrível comunidade
- [Leia](/p/faq) as Perguntas Frequentes (FAQ)

### Arquivos

- Screenshots
	- [home.png](/images/screenshots/home.png), [sinopse.png](/images/screenshots/sinopse.png), [tags.png](/images/screenshots/tags.png), [carregando.png](/images/screenshots/carregando.png), [assistindo.png](/images/screenshots/assistindo.png)
- Libreflix Logo
	- [.png](/images/media/libreflix-logo.png)
	- [.svg](/images/media/libreflix-logo.svg)
- Libreflix Icone
	- [.png](/images/media/libreflix-ico.png)
	- [.svg](/images/media/libreflix-ico.svg)
- Ver também: [Rabiscos](/c/rabiscos.html)

